/*1)функції дозволяють уникнути дублювання коду, тобто їх використовують для групування блоків коду, які виконують певну задачу, щоб можна було функції повторно використовувати*/
/*2)аргументи використовуються для передачі значень з однієї частини програми в іншу. Це дозволяє функції взаємодіяти з різними значеннями та здійснювати обчислення над ними*/
/*3)return використовується для повернення значення з функції. Коли виконується return, функція завершує своє виконання та повертає значення, яке було вказано у return*/

let firstNumber = prompt("enter first number");
let secondNumber = prompt("enter second number");

while (isNaN(firstNumber) || firstNumber === "" || isNaN(secondNumber) || secondNumber === "") {
    alert("Please enter valid information!");
    firstNumber = prompt("enter first number", firstNumber);
    secondNumber = prompt("enter second number", secondNumber);
}

let operation = prompt("enter math operation");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("the mathematical operator was entered incorrectly(+, -, *, /):");
}

const result = function (num1,num2,math){
    switch (math){
        case '+': return parseFloat(num1)+parseFloat(num2);
        case '-': return parseFloat(num1)-parseFloat(num2);
        case '/': return parseFloat(num1)/parseFloat(num2);
        case '*': return parseFloat(num1)*parseFloat(num2);
    }
}

console.log(result(firstNumber,secondNumber,operation));